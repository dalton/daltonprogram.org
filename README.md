# Sources behind http://daltonprogram.org.

## How to contribute

Fork this repository, make changes, submit a merge request.

After the merge request is accepted, it can take few minutes for the website to
refresh ([build status](https://gitlab.com/dalton/daltonprogram.org/pipelines)).

## How to build the website locally

Install [Jekyll](https://jekyllrb.com).

Then serve the website locally with:
```
$ jekyll serve
```
